/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package source;

import java.util.Stack;
import java.util.StringTokenizer;

/**
 *
 * @author Montzerrat Rivera
 */
public class infpersonal extends javax.swing.JFrame {
    
    public static String Sesion = "";

    /**
     * Creates new form infpersonal
     */
    public infpersonal() {
        initComponents();        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableper = new javax.swing.JTable();
        btnlim = new javax.swing.JButton();
        btnini = new javax.swing.JButton();
        combal = new javax.swing.JComboBox<>();
        btn2 = new javax.swing.JButton();
        txal = new javax.swing.JTextField();
        BtnRegresar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jLSesion = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Personal - Información");
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tableper.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Nombre", "Hora Entrada", "Hora Salida", "Horas Laborales", "Fecha"
            }
        ));
        jScrollPane1.setViewportView(tableper);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 170, 790, 290));

        btnlim.setText("Limpiar");
        getContentPane().add(btnlim, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 120, 70, -1));

        btnini.setText("Inicio");
        getContentPane().add(btnini, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 80, 70, -1));

        combal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Todas las carreras", "Ing. Sistemas Comp", "Ing. Industrial", "Ing. Electromecanica", "Ing. Mecatronica", "Ing. Ambiental", "Ing. Materiales", "Ing. Gestion Empresarial", "Ing. TICS", "Ing. Quimica", "Ing. Civil", "Lic. Administración" }));
        getContentPane().add(combal, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 110, 200, 30));

        btn2.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btn2.setText("Buscar");
        btn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2ActionPerformed(evt);
            }
        });
        getContentPane().add(btn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 110, -1, 30));

        txal.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        getContentPane().add(txal, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 110, 300, 30));

        BtnRegresar.setText("Regresar");
        BtnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnRegresarActionPerformed(evt);
            }
        });
        getContentPane().add(BtnRegresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 10, 90, 20));

        btnSalir.setText("Salir");
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });
        getContentPane().add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 10, 60, 20));

        jLSesion.setForeground(new java.awt.Color(0, 0, 0));
        jLSesion.setText("Usuario Sesion");
        getContentPane().add(jLSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 10, -1, -1));

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("Nombre: ");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, -1, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/FONDO.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 790, 420));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2ActionPerformed
        PullApart(txal.getText());
    }//GEN-LAST:event_btn2ActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSalirActionPerformed

    private void BtnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnRegresarActionPerformed
        menu regresar = new menu();
        menu.getSesion(Sesion);
        regresar.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnRegresarActionPerformed

    public void PullApart(String Name) {
        String[] parts = Name.split(" ");
        String n = "";
        String nombre = " ";
        String ApMaterno = parts[parts.length - 1];
        String ApPaterno = parts[parts.length - 2];
        for (int i = 3; i < parts.length + 1; i++) {
            if (parts[parts.length - i] != null) {
                n = n + parts[parts.length - i] + " ";
            }
        }

        StringTokenizer nom = new StringTokenizer(n);
        Stack pila = new Stack();

        while (nom.hasMoreTokens()) {
            pila.push(nom.nextToken());
        }

        while (!pila.empty()) {
            nombre = nombre + pila.pop() + " ";
        }

        System.out.println(parts.length);
        System.out.println("Apellido Materno: " + ApMaterno);
        System.out.println("Apellido Paterno: " + ApPaterno);
        System.out.println("Apellido nombre :" + nombre);

    }

    public static void getSesion(String user){
        Sesion = user;
        jLSesion.setText(user);
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(infpersonal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(infpersonal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(infpersonal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(infpersonal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new infpersonal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnRegresar;
    private javax.swing.JButton btn2;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnini;
    private javax.swing.JButton btnlim;
    private javax.swing.JComboBox<String> combal;
    private static javax.swing.JLabel jLSesion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableper;
    private javax.swing.JTextField txal;
    // End of variables declaration//GEN-END:variables
}
