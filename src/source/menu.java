/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package source;

import javax.swing.JOptionPane;

/**
 *
 * @author Montzerrat Rivera
 */
public class menu extends javax.swing.JFrame {

    public static String Sesion = "";
    /**
     * Creates new form menu
     */
    public menu() {
        initComponents();
        jComboBox1.setVisible(true);
        IR1.setVisible(true);        
    }

    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jComboBox1 = new javax.swing.JComboBox<>();
        jBoxPersoAlum = new javax.swing.JComboBox<>();
        BtnCloseSesion = new javax.swing.JButton();
        IR1 = new javax.swing.JButton();
        IR = new javax.swing.JButton();
        jL_User = new javax.swing.JLabel();
        jL_info2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jComboBox1.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Personal", "Alumnado" }));
        getContentPane().add(jComboBox1, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 280, 220, 30));

        jBoxPersoAlum.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jBoxPersoAlum.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Personal", "Alumnado" }));
        getContentPane().add(jBoxPersoAlum, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 170, 220, 30));

        BtnCloseSesion.setText("Cerrar Sesión");
        BtnCloseSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnCloseSesionActionPerformed(evt);
            }
        });
        getContentPane().add(BtnCloseSesion, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 350, -1, -1));

        IR1.setBackground(new java.awt.Color(51, 138, 46));
        IR1.setForeground(new java.awt.Color(255, 255, 255));
        IR1.setText("IR");
        IR1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IR1ActionPerformed(evt);
            }
        });
        getContentPane().add(IR1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 280, 70, 30));

        IR.setBackground(new java.awt.Color(51, 138, 46));
        IR.setForeground(new java.awt.Color(255, 255, 255));
        IR.setText("IR");
        IR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IRActionPerformed(evt);
            }
        });
        getContentPane().add(IR, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 170, 70, 30));

        jL_User.setForeground(new java.awt.Color(0, 0, 0));
        jL_User.setText("SESIÓN");
        getContentPane().add(jL_User, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 310, 110, 30));

        jL_info2.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        jL_info2.setForeground(new java.awt.Color(0, 0, 0));
        jL_info2.setText("Seleccione lo que deseé Agregar");
        getContentPane().add(jL_info2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 230, 380, 30));

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Seleccione lo que deseé visualizar");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 120, 380, 30));

        jLabel1.setBackground(new java.awt.Color(51, 138, 46));
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/FONDO.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 400));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void BtnCloseSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnCloseSesionActionPerformed
        index CSesion = new index();
        CSesion.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_BtnCloseSesionActionPerformed

    private void IRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IRActionPerformed
        switch (jBoxPersoAlum.getSelectedIndex()) {
            case 0:
                JOptionPane.showMessageDialog(null, "Elige una opcion");
                jBoxPersoAlum.requestFocus();
                break;
            case 1: //Personal
                infpersonal Accpersonal = new infpersonal();
                infpersonal.getSesion(Sesion);
                Accpersonal.setVisible(true);
                this.dispose();
                break;
            case 2: //Alumno
                infalumno AccAlumno = new infalumno();
                infalumno.getSesion(Sesion);
                AccAlumno.setVisible(true);
                this.dispose();
            default:
                throw new AssertionError();
        }
    }//GEN-LAST:event_IRActionPerformed

    private void IR1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IR1ActionPerformed
        switch (jComboBox1.getSelectedIndex()) {
            case 0: //Mensaje de Error
                
                break;
            case 1: //Personal
                
                break;
            case 2: //Alumnado
                
                break;
                
            default:
                throw new AssertionError();
        }
    }//GEN-LAST:event_IR1ActionPerformed
    
    public void ControlSesion(){
        jBoxPersoAlum.getSelectedIndex();
    }
    
    /**
     * 
     * @see getSesion
     * Este metodo obtiene la sesion que se inicio en el frame de index
     */
    public static void getSesion(String user){
        Sesion = user;
        jL_User.setText(user);
        if (!(user.equals("TESCoAdmin"))) {
            jComboBox1.setVisible(false);
            IR1.setVisible(false);
            jL_info2.setVisible(false);
        }
    }
    
    /**
     * @param args the command line arguments
     */
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new menu().setVisible(true);
            }
        });
    } 

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnCloseSesion;
    private javax.swing.JButton IR;
    private static javax.swing.JButton IR1;
    private javax.swing.JComboBox<String> jBoxPersoAlum;
    private static javax.swing.JComboBox<String> jComboBox1;
    private static javax.swing.JLabel jL_User;
    private static javax.swing.JLabel jL_info2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
}
