package source;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class index extends javax.swing.JFrame {

    static Connection con = ConnectionSQL.ConnSQL.getConnection();

    public index() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        get_id = new javax.swing.JTextField();
        get_pass = new javax.swing.JPasswordField();
        Btn_go = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Iniciar Sesion - TESCo");
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        get_id.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        get_id.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                get_idActionPerformed(evt);
            }
        });
        getContentPane().add(get_id, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 180, 410, 40));

        get_pass.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        get_pass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                get_passActionPerformed(evt);
            }
        });
        getContentPane().add(get_pass, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 260, 410, 40));

        Btn_go.setBackground(new java.awt.Color(75, 153, 55));
        Btn_go.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        Btn_go.setText("IR");
        Btn_go.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn_goActionPerformed(evt);
            }
        });
        getContentPane().add(Btn_go, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 320, 90, 30));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/FONDODAJMON.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, 460));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void Btn_goActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn_goActionPerformed
        Validate();
    }//GEN-LAST:event_Btn_goActionPerformed

    private void get_passActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_get_passActionPerformed

    }//GEN-LAST:event_get_passActionPerformed

    private void get_idActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_get_idActionPerformed
        
    }//GEN-LAST:event_get_idActionPerformed
    /**
     * @see Validate
     * Este metodo valida las credenciales para poder autorizar los datos o mostrar los datos
     *
     */
    public void Validate() {
            menu selec = new menu();
        try {
            String identidad = get_id.getText();
            String pass = get_pass.getText();
            
            PreparedStatement ps;
            ResultSet rs;
            ps = con.prepareStatement("SELECT Usuario FROM usuario WHERE Usuario = ? and Contraseña = ?"); 
            ps.setString(1, identidad);
            ps.setString(2, pass);
            rs=ps.executeQuery();
            
            if (rs.next()) {
              JOptionPane.showMessageDialog(null,"Bienbenido " + identidad);
              selec.setVisible(true);
              source.menu.getSesion(identidad);
              this.dispose();
            }else{
                JOptionPane.showMessageDialog(null,"ID o Password incorrectos");
                get_pass.setText("");
            }
        } catch (Exception e) {
            System.out.println("ID o Password incorrectos");
        }

    }

    /**
     * @param args the command line arguments
     */
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(index.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(index.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(index.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(index.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new index().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btn_go;
    private static javax.swing.JTextField get_id;
    private static javax.swing.JPasswordField get_pass;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
