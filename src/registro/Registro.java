package registro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

import java.util.Date;

import javax.swing.JOptionPane;

/**
 *
 * @author anmijurane
 */
public class Registro extends javax.swing.JFrame {

    static Connection con = ConnectionSQL.ConnSQL.getConnection();

    public Registro() {
        initComponents();
        jL_Error.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Registrar = new javax.swing.JButton();
        TxtFMatricula = new javax.swing.JTextField();
        jFielPass = new javax.swing.JPasswordField();
        jL_Error = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Registrar.setBackground(new java.awt.Color(51, 153, 0));
        Registrar.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        Registrar.setText("Registrar");
        Registrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RegistrarActionPerformed(evt);
            }
        });
        getContentPane().add(Registrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 320, -1, -1));

        TxtFMatricula.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        getContentPane().add(TxtFMatricula, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 190, 400, 40));

        jFielPass.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        getContentPane().add(jFielPass, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 260, 400, 40));

        jL_Error.setFont(new java.awt.Font("Dialog", 1, 10)); // NOI18N
        jL_Error.setForeground(new java.awt.Color(255, 51, 51));
        jL_Error.setText("¡Ingrese datos correctos!");
        getContentPane().add(jL_Error, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 324, 140, 20));

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/IMAGE/FONDODAJMON.jpg"))); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 800, -1));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    private void RegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RegistrarActionPerformed
        if (ValdCadena()) {
            if (ValidUser()) {
                System.out.println("Usuario Correcto");
                ValidarFecha();
                ClearUser();
            } else {
                //System.out.println("Credenciales Incorrectas");                
                Mensaje("Credenciales Incorrectas");
                ClearUser();
            }
        } else {
            Mensaje("Ingrese datos");
            System.out.println("Ingrese datos");
        }
    }//GEN-LAST:event_RegistrarActionPerformed

    public void ClearUser() {
        TxtFMatricula.setText("");
        jFielPass.setText("");        
    }

    public void Mensaje(String mnsj) {
        jL_Error.setText(mnsj);
    }

    public String DescPass() {
        String pass = new String(jFielPass.getPassword());
        System.out.println(pass);
        return pass;
    }

    /**
     *
     * Este metodo valida si existe un registro comparando la fecha de entrada
     * cuando encuentra una fecha del mismo dia, coloca en la base de datos una
     * fecha de salida.
     *
     * @see IngresarDatosEntrada
     *
     */
    public void ValidarFecha() {
        Connection con = ConnectionSQL.ConnSQL.getConnection();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        try {
            PreparedStatement ps;
            ResultSet lr;
            ps = con.prepareStatement("select Entrada from AlumnosIn where Matricula = ? ORDER BY Entrada DESC");
            ps.setString(1, TxtFMatricula.getText());
            lr = ps.executeQuery();
            if (lr.next()) { //if encontrar fecha                
                String DateActString = getDate(lr.getString("Entrada"));
                System.out.println(DateActString);
                if (DateActString.equals(getDate(timestamp.toString()))) {
                    ps = con.prepareStatement("UPDATE AlumnosIn SET Salida = ? where Matricula = ? and Entrada = ? ORDER BY Entrada DESC");
                    String FechaUpdate = timestamp.toString();
                    ps.setString(1, FechaUpdate);
                    ps.setString(2, TxtFMatricula.getText());
                    ps.setString(3, lr.getString("Entrada"));
                    ps.execute();
                    System.out.println("Son Iguales");
                    JOptionPane.showMessageDialog(null, "Acceso Correcto");
                    System.out.println("Acceso Correcto");
                    Mensaje("");
                    System.out.println(getDate(timestamp.toString()));
                } else {
                    System.out.println("No son iguales");
                    System.out.println("¡Acceso incorrecto!");
                    //JOptionPane.showMessageDialog(null, "Intente de Nuevo");
                    System.out.println(getDate(timestamp.toString()));                    
                    IngresarDatosEntrada();
                }//cierre if comparar fechas

            }// cierre de if encontrar fecha
            else {
                System.out.println("Acceso Correcto - Bienvenido");
                IngresarDatosEntrada();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     *
     * Este metodo ingresa los datos de entrada a la base de datos
     *
     */
    public void IngresarDatosEntrada() {
        Connection con = ConnectionSQL.ConnSQL.getConnection();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        try {
            PreparedStatement SP;
            ResultSet lr;
            SP = con.prepareStatement("select * from Alumno where id_matricula = ?");
            if (ValdCadena()) {
                SP.setString(1, TxtFMatricula.getText());
                lr = SP.executeQuery();
                if (lr.next()) {//Busqueda de matricula e inserción de datos a la tabla AlumnosIN
                    System.out.println(" Encontrado X2 ");
                    int mat = lr.getInt("id_matricula");
                    String Name = lr.getString("Nombres");
                    String LastName = lr.getString("Apellido_paterno");

                    SP = con.prepareStatement("INSERT INTO `AlumnosIn` (`Matricula`, `Nombres`, `Apellido_paterno`, `Entrada`) VALUES (?, ?, ?, ?)");
                    SP.setInt(1, mat);
                    SP.setString(2, Name);
                    SP.setString(3, LastName);
                    String FechaAIngresar = timestamp.toString();
                    SP.setString(4, FechaAIngresar);
                    SP.execute();
                    System.out.println("Ingresar datos entrada " + FechaAIngresar);
                    System.out.println("Acceso Correcto");
                    JOptionPane.showMessageDialog(null, "Acceso Correcto");
                    Mensaje("");
                }
            } else {
                System.out.println("Datos no ingresados");
                System.out.println("Acceso incorrecto");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String getDate(String Fecha) {
        String[] parts = Fecha.split(" ");
        String SoloFecha = parts[0];
        return SoloFecha;
    }

    public boolean ValdCadena() {
        if (TxtFMatricula.equals("") && DescPass().equals("")) {
            System.out.println("VACIO");
            return false;
        } else {
            System.out.println("LLENO");
            return true;
        }
    }

    public boolean ValidUser() {
        boolean UserExist = false;
        try {

            PreparedStatement ps;
            ResultSet rs;

            ps = con.prepareStatement("SELECT * FROM CredencialesAl WHERE UserAlumno = ? and PassAlumno = ?");
            ps.setString(1, TxtFMatricula.getText());
            ps.setString(2, DescPass());
            rs = ps.executeQuery();

            if (rs.next()) {//validacion de usuario                
                //IngresarDatosEntrada();
                UserExist = true;
            } else { //if validación de usuario
                UserExist = false;
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return UserExist;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Registro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Registrar;
    private javax.swing.JTextField TxtFMatricula;
    private javax.swing.JPasswordField jFielPass;
    private javax.swing.JLabel jL_Error;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables
}
