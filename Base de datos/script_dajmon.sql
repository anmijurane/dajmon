-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 13-11-2019 a las 05:04:39
-- Versión del servidor: 8.0.13-4
-- Versión de PHP: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `EMeiVYIRzd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Alumno`
--

CREATE TABLE `Alumno` (
  `id_matricula` int(10) NOT NULL,
  `Nombre1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '---',
  `Apellido_paterno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido_materno` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Asignatura`
--

CREATE TABLE `Asignatura` (
  `id_asig` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE `carrera` (
  `id_carrera` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Detalle_alumno`
--

CREATE TABLE `Detalle_alumno` (
  `id_matricula` int(10) NOT NULL,
  `id_turno` int(10) NOT NULL,
  `id_grupo` int(10) NOT NULL,
  `id_carrera` int(10) NOT NULL,
  `Fecha_llegada` date DEFAULT NULL,
  `Fecha_salida` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Detalle_docente`
--

CREATE TABLE `Detalle_docente` (
  `id_docente` int(10) NOT NULL,
  `id_turno` int(10) NOT NULL,
  `id_grupo` int(10) NOT NULL,
  `id_carrera` int(10) NOT NULL,
  `id_salon` int(10) NOT NULL,
  `id_edificio` int(10) NOT NULL,
  `Fecha_llegada` date DEFAULT NULL,
  `Fecha_salida` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Detalle_jefes`
--

CREATE TABLE `Detalle_jefes` (
  `id_jefes` int(10) NOT NULL,
  `id_turno` int(10) NOT NULL,
  `id_carrera` int(10) NOT NULL,
  `id_edificio` int(10) NOT NULL,
  `Fecha_llegada` date DEFAULT NULL,
  `Fecha_salida` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Detalle_personal`
--

CREATE TABLE `Detalle_personal` (
  `id_personal` int(10) NOT NULL,
  `id_turno` int(10) NOT NULL,
  `id_carrera` int(10) NOT NULL,
  `id_edificio` int(10) NOT NULL,
  `Fecha_llegada` date DEFAULT NULL,
  `Fecha_salida` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Docente`
--

CREATE TABLE `Docente` (
  `id_docente` int(10) NOT NULL,
  `Nombre1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '---',
  `Apellido_paterno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido_materno` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Edificio`
--

CREATE TABLE `Edificio` (
  `id_edificio` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE `grupo` (
  `id_grupo` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Jefes_division`
--

CREATE TABLE `Jefes_division` (
  `id_jefes` int(10) NOT NULL,
  `Nombre1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '---',
  `Apellido_paterno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido_materno` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Personal`
--

CREATE TABLE `Personal` (
  `id_personal` int(10) NOT NULL,
  `Nombre1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '---',
  `Apellido_paterno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido_materno` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Salon`
--

CREATE TABLE `Salon` (
  `id_salon` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Turno`
--

CREATE TABLE `Turno` (
  `id_turno` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Alumno`
--
ALTER TABLE `Alumno`
  ADD PRIMARY KEY (`id_matricula`);

--
-- Indices de la tabla `Asignatura`
--
ALTER TABLE `Asignatura`
  ADD PRIMARY KEY (`id_asig`);

--
-- Indices de la tabla `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`id_carrera`);

--
-- Indices de la tabla `Detalle_alumno`
--
ALTER TABLE `Detalle_alumno`
  ADD KEY `id_matricula` (`id_matricula`),
  ADD KEY `id_turno` (`id_turno`),
  ADD KEY `id_grupo` (`id_grupo`),
  ADD KEY `id_carrera` (`id_carrera`);

--
-- Indices de la tabla `Detalle_docente`
--
ALTER TABLE `Detalle_docente`
  ADD KEY `id_docente` (`id_docente`),
  ADD KEY `id_turno` (`id_turno`),
  ADD KEY `id_grupo` (`id_grupo`),
  ADD KEY `id_carrera` (`id_carrera`),
  ADD KEY `id_salon` (`id_salon`),
  ADD KEY `id_edificio` (`id_edificio`);

--
-- Indices de la tabla `Detalle_jefes`
--
ALTER TABLE `Detalle_jefes`
  ADD KEY `id_jefes` (`id_jefes`),
  ADD KEY `id_turno` (`id_turno`),
  ADD KEY `id_carrera` (`id_carrera`),
  ADD KEY `id_edificio` (`id_edificio`);

--
-- Indices de la tabla `Detalle_personal`
--
ALTER TABLE `Detalle_personal`
  ADD KEY `id_personal` (`id_personal`),
  ADD KEY `id_turno` (`id_turno`),
  ADD KEY `id_carrera` (`id_carrera`),
  ADD KEY `id_edificio` (`id_edificio`);

--
-- Indices de la tabla `Docente`
--
ALTER TABLE `Docente`
  ADD PRIMARY KEY (`id_docente`);

--
-- Indices de la tabla `Edificio`
--
ALTER TABLE `Edificio`
  ADD PRIMARY KEY (`id_edificio`);

--
-- Indices de la tabla `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id_grupo`);

--
-- Indices de la tabla `Jefes_division`
--
ALTER TABLE `Jefes_division`
  ADD PRIMARY KEY (`id_jefes`);

--
-- Indices de la tabla `Personal`
--
ALTER TABLE `Personal`
  ADD PRIMARY KEY (`id_personal`);

--
-- Indices de la tabla `Salon`
--
ALTER TABLE `Salon`
  ADD PRIMARY KEY (`id_salon`);

--
-- Indices de la tabla `Turno`
--
ALTER TABLE `Turno`
  ADD PRIMARY KEY (`id_turno`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `Detalle_alumno`
--
ALTER TABLE `Detalle_alumno`
  ADD CONSTRAINT `Detalle_alumno_ibfk_1` FOREIGN KEY (`id_matricula`) REFERENCES `Alumno` (`id_matricula`),
  ADD CONSTRAINT `Detalle_alumno_ibfk_2` FOREIGN KEY (`id_turno`) REFERENCES `Turno` (`id_turno`),
  ADD CONSTRAINT `Detalle_alumno_ibfk_3` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`),
  ADD CONSTRAINT `Detalle_alumno_ibfk_4` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`);

--
-- Filtros para la tabla `Detalle_docente`
--
ALTER TABLE `Detalle_docente`
  ADD CONSTRAINT `Detalle_docente_ibfk_1` FOREIGN KEY (`id_docente`) REFERENCES `Docente` (`id_docente`),
  ADD CONSTRAINT `Detalle_docente_ibfk_2` FOREIGN KEY (`id_turno`) REFERENCES `Turno` (`id_turno`),
  ADD CONSTRAINT `Detalle_docente_ibfk_3` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`),
  ADD CONSTRAINT `Detalle_docente_ibfk_4` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`),
  ADD CONSTRAINT `Detalle_docente_ibfk_5` FOREIGN KEY (`id_salon`) REFERENCES `Salon` (`id_salon`),
  ADD CONSTRAINT `Detalle_docente_ibfk_6` FOREIGN KEY (`id_edificio`) REFERENCES `Edificio` (`id_edificio`);

--
-- Filtros para la tabla `Detalle_jefes`
--
ALTER TABLE `Detalle_jefes`
  ADD CONSTRAINT `Detalle_jefes_ibfk_1` FOREIGN KEY (`id_jefes`) REFERENCES `Jefes_division` (`id_jefes`),
  ADD CONSTRAINT `Detalle_jefes_ibfk_2` FOREIGN KEY (`id_turno`) REFERENCES `Turno` (`id_turno`),
  ADD CONSTRAINT `Detalle_jefes_ibfk_3` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`),
  ADD CONSTRAINT `Detalle_jefes_ibfk_4` FOREIGN KEY (`id_edificio`) REFERENCES `Edificio` (`id_edificio`);

--
-- Filtros para la tabla `Detalle_personal`
--
ALTER TABLE `Detalle_personal`
  ADD CONSTRAINT `Detalle_personal_ibfk_1` FOREIGN KEY (`id_personal`) REFERENCES `Personal` (`id_personal`),
  ADD CONSTRAINT `Detalle_personal_ibfk_2` FOREIGN KEY (`id_turno`) REFERENCES `Turno` (`id_turno`),
  ADD CONSTRAINT `Detalle_personal_ibfk_3` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`),
  ADD CONSTRAINT `Detalle_personal_ibfk_4` FOREIGN KEY (`id_edificio`) REFERENCES `Edificio` (`id_edificio`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
