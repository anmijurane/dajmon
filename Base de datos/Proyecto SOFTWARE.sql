create database software;
use software;

CREATE TABLE Alumno(
id_matricula INT (10) NOT NULL,
Nombre1 VARCHAR (100) NOT NULL,
Nombre2 VARCHAR (100) NOT NULL DEFAULT '---',
Apellido_paterno VARCHAR (100) NOT NULL,
Apellido_materno VARCHAR (100) NOT NULL,
PRIMARY KEY(id_matricula));
 
INSERT INTO Alumno (id_matricula, Nombre1, Nombre2, Apellido_paterno, Apellido_materno) VALUES 
(201621468, 'Nini', 'Guadalupe', 'Gastaldi', 'Orozco'),
(201621115, 'Jair', DEFAULT, 'Jimenez', 'Garcia');
 
CREATE TABLE Docente(
id_docente INT (10) NOT NULL,
Nombre1 VARCHAR (100) NOT NULL,
Nombre2 VARCHAR (100) NOT NULL DEFAULT '---',
Apellido_paterno VARCHAR (100) NOT NULL,
Apellido_materno VARCHAR (100) NOT NULL,
PRIMARY KEY(id_docente)); 

INSERT INTO Docente(id_docente, Nombre1, Nombre2, Apellido_paterno, Apellido_materno) VALUES
(013265, 'Juan', DEFAULT, 'Maldonado','Corona'),
(156237, 'Roberto', 'Carlos', 'Muñoz', 'Celaya');

CREATE TABLE Personal(
id_personal INT (10) NOT NULL,
Nombre1 VARCHAR (100) NOT NULL,
Nombre2 VARCHAR (100) NOT NULL DEFAULT '---',
Apellido_paterno VARCHAR (100) NOT NULL,
Apellido_materno VARCHAR (100) NOT NULL,
PRIMARY KEY(id_personal)); 

INSERT INTO personal(id_personal, Nombre1, Nombre2, Apellido_paterno, Apellido_materno) VALUES
(35421, 'Maria', DEFAULT, 'Lopez', 'Garcia'),
(65389, 'Daniel', 'Antonio', 'Cruz', 'Bustamante');

CREATE TABLE Jefes_division(
id_jefes INT (10) NOT NULL,
Nombre1 VARCHAR (100) NOT NULL,
Nombre2 VARCHAR (100) NOT NULL DEFAULT '---',
Apellido_paterno VARCHAR (100) NOT NULL,
Apellido_materno VARCHAR (100) NOT NULL,
PRIMARY KEY(id_jefes)); 

INSERT INTO jefes_division(id_jefes, Nombre1, Nombre2, Apellido_paterno, Apellido_materno) VALUES
(45213, 'Israel', DEFAULT, 'Zermeño', 'Caballero');


CREATE TABLE carrera(
  id_carrera int(10) NOT NULL,
  Descripcion VARCHAR (100) NOT NULL,
  PRIMARY KEY (id_carrera));
  
  INSERT INTO carrera (id_carrera,Descripcion) VALUES 
 (3,'Ingenieria en Sistemas Computacionales'),
 (12,'Ingenieria en tecnologias de la informacion y comunicacion');
  
  CREATE TABLE grupo(
  id_grupo int(10) NOT NULL,
  Descripcion VARCHAR (100) NOT NULL,
  PRIMARY KEY (id_grupo));
  
  INSERT INTO grupo (id_grupo,Descripcion) VALUES 
 (3712,'Treinta-siete, doce'),
 (3721,'Treinta-siete, veinti-uno');
  
  CREATE TABLE Turno(
  id_turno int(10) NOT NULL,
  Descripcion VARCHAR (100) NOT NULL,
  PRIMARY KEY (id_turno));
  
  INSERT INTO turno (id_turno,Descripcion) VALUES 
 (1,'Matutino'),
 (2,'Vespertino');
  
  CREATE TABLE semestre (
  id_semestre int(10) NOT NULL,
  Descripcion text,
  PRIMARY KEY (id_semestre));

INSERT INTO semestre (id_semestre,Descripcion) VALUES 
 (7,'Septimo semestre'),
 (8,'Octavo semestre');
 
  CREATE TABLE Salon(
  id_salon int(10) NOT NULL,
  Descripcion VARCHAR (100) NOT NULL,
  PRIMARY KEY (id_salon));
  
  INSERT INTO Salon (id_salon, Descripcion) VALUES
  (01, 'A02'),
  (03, 'Lab CISCO');
  
  CREATE TABLE Asignatura(
  id_asig int(10) NOT NULL,
  Descripcion VARCHAR (100) NOT NULL,
  No_creditos int (10),
  PRIMARY KEY (id_asig));
  
  INSERT INTO asignatura (id_asig,descripcion,No_creditos) VALUES 
 (20,'Lenguajes y automatas II',5),
 (50,'Ingenieria de Software',4);
  

  CREATE TABLE Edificio(
  id_edificio int(10) NOT NULL,
  Descripcion VARCHAR (100) NOT NULL,
  PRIMARY KEY (id_edificio));
  
  INSERT INTO Edificio (id_edificio, Descripcion) VALUES
  (01, 'Edifcio A'),
  (05, 'Edificio E');
  
  CREATE TABLE Detalle_alumno(
  id_matricula INT (10) NOT NULL,
  FOREIGN KEY (id_matricula) REFERENCES Alumno(id_matricula),
  id_turno INT (10) NOT NULL,
  FOREIGN KEY (id_turno) REFERENCES Turno(id_turno),
  id_grupo INT (10) NOT NULL,
  FOREIGN KEY (id_grupo) REFERENCES grupo(id_grupo),
   id_carrera INT (10) NOT NULL,
  FOREIGN KEY (id_carrera) REFERENCES carrera(id_carrera),
  id_semestre INT (10) NOT NULL,
  FOREIGN KEY (id_semestre) REFERENCES Semestre(id_semestre),
  Fecha_llegada DATE,
  Fecha_salida DATE);
  
  INSERT INTO Detalle_alumno (id_matricula, id_turno, id_grupo, id_carrera, id_semestre, Fecha_llegada, Fecha_salida) VALUES
  (201621468, 2, 3721, 3, 7, '2019-11-05', '2019-11-05'),
  (201621115, 2, 3721, 3, 7, '2019-11-05', '2019-11-05');
  
  CREATE TABLE Detalle_docente(
  id_docente INT (10) NOT NULL,
  FOREIGN KEY (id_docente) REFERENCES Docente(id_docente),
  id_turno INT (10) NOT NULL,
  FOREIGN KEY (id_turno) REFERENCES Turno(id_turno),
  id_grupo INT (10) NOT NULL,
  FOREIGN KEY (id_grupo) REFERENCES grupo(id_grupo),
   id_carrera INT (10) NOT NULL,
  FOREIGN KEY (id_carrera) REFERENCES carrera(id_carrera),
   id_salon INT (10) NOT NULL,
   id_semestre INT (10) NOT NULL,
  FOREIGN KEY (id_semestre) REFERENCES Semestre(id_semestre),
  FOREIGN KEY (id_salon) REFERENCES Salon(id_salon),
   id_edificio INT (10) NOT NULL,
  FOREIGN KEY (id_edificio) REFERENCES Edificio(id_edificio),
  Fecha_llegada DATE,
  Fecha_salida DATE);
  
  INSERT INTO Detalle_docente(id_docente, id_turno, id_grupo, id_carrera, id_salon, id_semestre, id_edificio, 	Fecha_llegada, Fecha_salida) VALUES
  (013265, 2, 3721, 3,01,7,01, '2019-11-14', '2019-11-14'),
  (156237, 2, 3721, 3,01,7,01, '2019-11-14', '2019-11-14');

   CREATE TABLE Detalle_personal(
  id_personal INT (10) NOT NULL,
  FOREIGN KEY (id_personal) REFERENCES Personal(id_personal),
  id_turno INT (10) NOT NULL,
  FOREIGN KEY (id_turno) REFERENCES Turno(id_turno),
   id_carrera INT (10) NOT NULL,
  FOREIGN KEY (id_carrera) REFERENCES carrera(id_carrera),
   id_edificio INT (10) NOT NULL,
  FOREIGN KEY (id_edificio) REFERENCES Edificio(id_edificio),
  Fecha_llegada DATE,
  Fecha_salida DATE);
  
  INSERT INTO detalle_personal(id_personal, id_turno, id_carrera, id_edificio, Fecha_llegada, Fecha_salida) VALUES
(35421, 2, 3, 05, '2019-11-13', '2019-11-13'),
(65389, 2, 12, 01, '2019-11-13', '2019-11-13');
  
  CREATE TABLE Detalle_jefes(
  id_jefes INT (10) NOT NULL,
  FOREIGN KEY (id_jefes) REFERENCES Jefes_division(id_jefes),
  id_turno INT (10) NOT NULL,
  FOREIGN KEY (id_turno) REFERENCES Turno(id_turno),
   id_carrera INT (10) NOT NULL,
  FOREIGN KEY (id_carrera) REFERENCES carrera(id_carrera),
   id_edificio INT (10) NOT NULL,
  FOREIGN KEY (id_edificio) REFERENCES Edificio(id_edificio),
  Fecha_llegada DATE,
  Fecha_salida DATE);
  
  INSERT INTO detalle_jefes(id_jefes, id_turno, id_carrera, id_edificio, Fecha_llegada, Fecha_salida) VALUES
(45213, 1, 3, 05,'2019-11-13', '2019-11-13');
  
 