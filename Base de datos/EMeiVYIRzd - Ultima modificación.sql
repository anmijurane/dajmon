-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 26, 2019 at 07:31 PM
-- Server version: 8.0.13-4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `EMeiVYIRzd`
--

-- --------------------------------------------------------

--
-- Table structure for table `Alumno`
--

CREATE TABLE `Alumno` (
  `id_matricula` int(10) NOT NULL,
  `Nombre1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '---',
  `Apellido_paterno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido_materno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sexo` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Alumno`
--

INSERT INTO `Alumno` (`id_matricula`, `Nombre1`, `Nombre2`, `Apellido_paterno`, `Apellido_materno`, `sexo`) VALUES
(201601005, 'Diana', 'Elizabeth', 'Fuentes', 'Lopez', 'Mujer'),
(201610112, 'Octavio', '---', 'Gonzalez', 'Lopez', 'Hombre'),
(201620096, 'Miguel', 'Andres', 'Jurado', 'Negrete', 'Hombre'),
(201620419, 'Montzerrat', 'Guadalupe', 'Gonzalez', 'Rivera', 'Mujer'),
(201621105, 'Jair', '---', 'Jimenez', 'Garcia', 'Hombre'),
(201621468, 'Nini', 'Guadalupe', 'Gastaldi', 'Orozco', 'Mujer');

-- --------------------------------------------------------

--
-- Table structure for table `Asignatura`
--

CREATE TABLE `Asignatura` (
  `id_asig` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Asignatura`
--

INSERT INTO `Asignatura` (`id_asig`, `Descripcion`) VALUES
(36113, 'INGENIERÍA DE SOFTWARE'),
(37122, 'LENGUAJES Y AUTOMATAS II'),
(37123, 'SISTEMAS PROGRAMABLES'),
(37125, 'TALLER DE INVESTIGACIÓN II'),
(38127, 'ADMINISTRACIÓN DE REDES'),
(38128, 'PROGRAMACIÓN LÓGICA Y FUNCIONAL');

-- --------------------------------------------------------

--
-- Table structure for table `carrera`
--

CREATE TABLE `carrera` (
  `id_carrera` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `carrera`
--

INSERT INTO `carrera` (`id_carrera`, `Descripcion`) VALUES
(3, 'INGENIERÍA EN SISTEMAS COMPUTACIONALES'),
(4, 'INGENIERÍA INDUSTRIAL'),
(5, 'LICENCIATURA EN ADMINISTRACIÓN'),
(6, 'INGENIERÍA QUÍMICA'),
(7, 'INGENIERÍA EN GESTIÓN EMPRESARIAL'),
(8, 'INGENIERÍA MECATRÓNICA'),
(9, 'INGENIERÍA EN TECNOLOGÍAS DE LA INFORMACIÓN Y COMUNICACIÓN');

-- --------------------------------------------------------

--
-- Table structure for table `Detalle_alumno`
--

CREATE TABLE `Detalle_alumno` (
  `id_matricula` int(10) NOT NULL,
  `id_turno` int(10) NOT NULL,
  `id_grupo` int(10) NOT NULL,
  `id_carrera` int(10) NOT NULL,
  `Info_Llegada` timestamp NULL DEFAULT NULL,
  `Info_salida` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Detalle_alumno`
--

INSERT INTO `Detalle_alumno` (`id_matricula`, `id_turno`, `id_grupo`, `id_carrera`, `Info_Llegada`, `Info_salida`) VALUES
(201601005, 2, 3721, 3, '2019-12-26 00:00:00', '2019-12-26 00:00:00'),
(201610112, 2, 3721, 3, '2019-12-26 00:00:00', '2019-12-26 00:00:00'),
(201620096, 2, 3721, 3, '2019-12-26 00:00:00', '2019-12-26 00:00:00'),
(201620419, 2, 3721, 3, '2019-12-26 00:00:00', '2019-12-26 00:00:00'),
(201621105, 2, 3721, 3, '2019-12-26 00:00:00', '2019-12-26 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `Detalle_docente`
--

CREATE TABLE `Detalle_docente` (
  `id_docente` int(10) NOT NULL,
  `id_turno` int(10) NOT NULL,
  `id_grupo` int(10) NOT NULL,
  `id_carrera` int(10) NOT NULL,
  `id_salon` int(10) NOT NULL,
  `id_edificio` int(10) NOT NULL,
  `Fecha_llegada` date DEFAULT NULL,
  `Fecha_salida` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Detalle_docente`
--

INSERT INTO `Detalle_docente` (`id_docente`, `id_turno`, `id_grupo`, `id_carrera`, `id_salon`, `id_edificio`, `Fecha_llegada`, `Fecha_salida`) VALUES
(3259754, 2, 3721, 3, 12, 1, '2019-11-26', '2019-11-26'),
(4952631, 2, 3721, 3, 12, 1, '2019-11-26', '2019-11-26');

-- --------------------------------------------------------

--
-- Table structure for table `Detalle_jefes`
--

CREATE TABLE `Detalle_jefes` (
  `id_jefes` int(10) NOT NULL,
  `id_turno` int(10) NOT NULL,
  `id_carrera` int(10) NOT NULL,
  `id_edificio` int(10) NOT NULL,
  `Fecha_llegada` date DEFAULT NULL,
  `Fecha_salida` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Detalle_personal`
--

CREATE TABLE `Detalle_personal` (
  `id_personal` int(10) NOT NULL,
  `id_turno` int(10) NOT NULL,
  `id_carrera` int(10) NOT NULL,
  `id_edificio` int(10) NOT NULL,
  `Fecha_llegada` date DEFAULT NULL,
  `Fecha_salida` date DEFAULT NULL,
  `hora_llegada` time DEFAULT NULL,
  `hora_salida` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Detalle_personal`
--

INSERT INTO `Detalle_personal` (`id_personal`, `id_turno`, `id_carrera`, `id_edificio`, `Fecha_llegada`, `Fecha_salida`, `hora_llegada`, `hora_salida`) VALUES
(123, 1, 3, 1, '2019-11-21', '2019-11-21', '07:00:00', '18:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `Docente`
--

CREATE TABLE `Docente` (
  `id_docente` int(10) NOT NULL,
  `Nombre1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '---',
  `Apellido_paterno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido_materno` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Docente`
--

INSERT INTO `Docente` (`id_docente`, `Nombre1`, `Nombre2`, `Apellido_paterno`, `Apellido_materno`) VALUES
(3259754, 'Juan', '---', 'Maldonado', 'Corona'),
(4952631, 'Silvia', 'Carina', 'Cu', 'Lara');

-- --------------------------------------------------------

--
-- Table structure for table `Edificio`
--

CREATE TABLE `Edificio` (
  `id_edificio` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Edificio`
--

INSERT INTO `Edificio` (`id_edificio`, `Descripcion`) VALUES
(1, 'Edificio A'),
(5, 'Edificio E');

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE `grupo` (
  `id_grupo` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `grupo`
--

INSERT INTO `grupo` (`id_grupo`, `Descripcion`) VALUES
(3712, 'Treinta-siete, doce'),
(3721, 'Treinta-siete, veinti-uno');

-- --------------------------------------------------------

--
-- Table structure for table `Jefes_division`
--

CREATE TABLE `Jefes_division` (
  `id_jefes` int(10) NOT NULL,
  `Nombre1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '---',
  `Apellido_paterno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido_materno` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Jefes_division`
--

INSERT INTO `Jefes_division` (`id_jefes`, `Nombre1`, `Nombre2`, `Apellido_paterno`, `Apellido_materno`) VALUES
(156259, 'Israel', '---', 'Zermello', 'Caballero');

-- --------------------------------------------------------

--
-- Table structure for table `Personal`
--

CREATE TABLE `Personal` (
  `id_personal` int(10) NOT NULL,
  `Nombre1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Nombre2` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '---',
  `Apellido_paterno` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Apellido_materno` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Personal`
--

INSERT INTO `Personal` (`id_personal`, `Nombre1`, `Nombre2`, `Apellido_paterno`, `Apellido_materno`) VALUES
(123, 'Francisco', '---', 'Fernandez', 'Gomez');

-- --------------------------------------------------------

--
-- Table structure for table `Salon`
--

CREATE TABLE `Salon` (
  `id_salon` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Salon`
--

INSERT INTO `Salon` (`id_salon`, `Descripcion`) VALUES
(12, 'A0'),
(52, 'Laboratirio CISCO');

-- --------------------------------------------------------

--
-- Table structure for table `semestre`
--

CREATE TABLE `semestre` (
  `id_semestre` int(10) NOT NULL,
  `Descripcion` text COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `semestre`
--

INSERT INTO `semestre` (`id_semestre`, `Descripcion`) VALUES
(7, 'Septimo semestre'),
(8, 'Octavo semestre');

-- --------------------------------------------------------

--
-- Table structure for table `Turno`
--

CREATE TABLE `Turno` (
  `id_turno` int(10) NOT NULL,
  `Descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Turno`
--

INSERT INTO `Turno` (`id_turno`, `Descripcion`) VALUES
(1, 'Matutino'),
(2, 'Vespertino');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `Usuario` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Contraseña` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sexo` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`Usuario`, `Contraseña`, `sexo`) VALUES
('Administracion', 'adb58fd5f1faeb6071a48200e18bb9da', NULL),
('156259Israel', 'Caballero156259', NULL),
('TESCoAdmin', 'RootTESCo', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Alumno`
--
ALTER TABLE `Alumno`
  ADD PRIMARY KEY (`id_matricula`);

--
-- Indexes for table `Asignatura`
--
ALTER TABLE `Asignatura`
  ADD PRIMARY KEY (`id_asig`);

--
-- Indexes for table `carrera`
--
ALTER TABLE `carrera`
  ADD PRIMARY KEY (`id_carrera`);

--
-- Indexes for table `Detalle_alumno`
--
ALTER TABLE `Detalle_alumno`
  ADD KEY `id_matricula` (`id_matricula`),
  ADD KEY `id_turno` (`id_turno`),
  ADD KEY `id_grupo` (`id_grupo`),
  ADD KEY `id_carrera` (`id_carrera`);

--
-- Indexes for table `Detalle_docente`
--
ALTER TABLE `Detalle_docente`
  ADD KEY `id_docente` (`id_docente`),
  ADD KEY `id_turno` (`id_turno`),
  ADD KEY `id_grupo` (`id_grupo`),
  ADD KEY `id_carrera` (`id_carrera`),
  ADD KEY `id_salon` (`id_salon`),
  ADD KEY `id_edificio` (`id_edificio`);

--
-- Indexes for table `Detalle_jefes`
--
ALTER TABLE `Detalle_jefes`
  ADD KEY `id_jefes` (`id_jefes`),
  ADD KEY `id_turno` (`id_turno`),
  ADD KEY `id_carrera` (`id_carrera`),
  ADD KEY `id_edificio` (`id_edificio`);

--
-- Indexes for table `Detalle_personal`
--
ALTER TABLE `Detalle_personal`
  ADD KEY `id_personal` (`id_personal`),
  ADD KEY `id_turno` (`id_turno`),
  ADD KEY `id_carrera` (`id_carrera`),
  ADD KEY `id_edificio` (`id_edificio`);

--
-- Indexes for table `Docente`
--
ALTER TABLE `Docente`
  ADD PRIMARY KEY (`id_docente`);

--
-- Indexes for table `Edificio`
--
ALTER TABLE `Edificio`
  ADD PRIMARY KEY (`id_edificio`);

--
-- Indexes for table `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id_grupo`);

--
-- Indexes for table `Jefes_division`
--
ALTER TABLE `Jefes_division`
  ADD PRIMARY KEY (`id_jefes`);

--
-- Indexes for table `Personal`
--
ALTER TABLE `Personal`
  ADD PRIMARY KEY (`id_personal`);

--
-- Indexes for table `Salon`
--
ALTER TABLE `Salon`
  ADD PRIMARY KEY (`id_salon`);

--
-- Indexes for table `semestre`
--
ALTER TABLE `semestre`
  ADD PRIMARY KEY (`id_semestre`);

--
-- Indexes for table `Turno`
--
ALTER TABLE `Turno`
  ADD PRIMARY KEY (`id_turno`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Detalle_alumno`
--
ALTER TABLE `Detalle_alumno`
  ADD CONSTRAINT `Detalle_alumno_ibfk_1` FOREIGN KEY (`id_matricula`) REFERENCES `Alumno` (`id_matricula`),
  ADD CONSTRAINT `Detalle_alumno_ibfk_2` FOREIGN KEY (`id_turno`) REFERENCES `Turno` (`id_turno`),
  ADD CONSTRAINT `Detalle_alumno_ibfk_3` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`),
  ADD CONSTRAINT `Detalle_alumno_ibfk_4` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`);

--
-- Constraints for table `Detalle_docente`
--
ALTER TABLE `Detalle_docente`
  ADD CONSTRAINT `Detalle_docente_ibfk_1` FOREIGN KEY (`id_docente`) REFERENCES `Docente` (`id_docente`),
  ADD CONSTRAINT `Detalle_docente_ibfk_2` FOREIGN KEY (`id_turno`) REFERENCES `Turno` (`id_turno`),
  ADD CONSTRAINT `Detalle_docente_ibfk_3` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id_grupo`),
  ADD CONSTRAINT `Detalle_docente_ibfk_4` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`),
  ADD CONSTRAINT `Detalle_docente_ibfk_5` FOREIGN KEY (`id_salon`) REFERENCES `Salon` (`id_salon`),
  ADD CONSTRAINT `Detalle_docente_ibfk_6` FOREIGN KEY (`id_edificio`) REFERENCES `Edificio` (`id_edificio`);

--
-- Constraints for table `Detalle_jefes`
--
ALTER TABLE `Detalle_jefes`
  ADD CONSTRAINT `Detalle_jefes_ibfk_1` FOREIGN KEY (`id_jefes`) REFERENCES `Jefes_division` (`id_jefes`),
  ADD CONSTRAINT `Detalle_jefes_ibfk_2` FOREIGN KEY (`id_turno`) REFERENCES `Turno` (`id_turno`),
  ADD CONSTRAINT `Detalle_jefes_ibfk_3` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`),
  ADD CONSTRAINT `Detalle_jefes_ibfk_4` FOREIGN KEY (`id_edificio`) REFERENCES `Edificio` (`id_edificio`);

--
-- Constraints for table `Detalle_personal`
--
ALTER TABLE `Detalle_personal`
  ADD CONSTRAINT `Detalle_personal_ibfk_1` FOREIGN KEY (`id_personal`) REFERENCES `Personal` (`id_personal`),
  ADD CONSTRAINT `Detalle_personal_ibfk_2` FOREIGN KEY (`id_turno`) REFERENCES `Turno` (`id_turno`),
  ADD CONSTRAINT `Detalle_personal_ibfk_3` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id_carrera`),
  ADD CONSTRAINT `Detalle_personal_ibfk_4` FOREIGN KEY (`id_edificio`) REFERENCES `Edificio` (`id_edificio`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
